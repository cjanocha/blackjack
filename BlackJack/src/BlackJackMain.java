import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class BlackJackMain extends Application{
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage window) throws Exception {
		Alert alert=new Alert(AlertType.CONFIRMATION);
		alert.setTitle("JFX BlackJack");
		alert.setHeaderText("BlackJack");
		alert.setContentText("The object of BlackJack is to obtain a hand of cards as close to or equalling 21. Face cards are worth 10, and aces are worth 1 or 11 (your choice).");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get()==ButtonType.OK){	
			
		}else{
			System.exit(1);
		}
		
		BorderPane
		GridPane grid = new GridPane();
		
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setAlignment(Pos.BOTTOM_LEFT);
        grid.add(new Button("Hit"), 0, 5);
        grid.add(new Button("Stay"), 1, 5);
        grid.add(new Label("Your Total"), 0, 0);
		Scene scene = new Scene(grid, 300, 300);
		window.setScene(scene);
		window.show();
		
}
}
	


